<?php

namespace ContextualCode\Threads;

use Worker as BaseWorker;
use Volatile;

/**
 * Extends Worker class from pthreads PHP extension
 *
 * @author Serhey Dolgushev <serhey@contextualcode.com>
 */
class Worker extends BaseWorker
{
    private $data;

    public function __construct(Volatile $data)
    {
        $this->data = $data;
    }
}
