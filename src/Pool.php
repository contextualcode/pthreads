<?php

namespace ContextualCode\Threads;

use Pool as BasePool;

/**
 * Extends Pool class from pthreads PHP extension
 *
 * @author Serhey Dolgushev <serhey@contextualcode.com>
 */
class Pool extends BasePool
{
    public function process(): void
    {
        while ($this->collect());

        $this->shutdown();
    }
}
